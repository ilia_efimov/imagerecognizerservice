FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y git
RUN git clone https://ilia_efimov@bitbucket.org/ilia_efimov/imagerecognizerservice.git
RUN apt install -y maven
RUN mvn install -f imagerecognizerservice/pom.xml

WORKDIR /imagerecognizerservice
