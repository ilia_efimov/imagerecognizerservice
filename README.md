Example of using tensorflow with java

Build docker image from sources: docker build -t "img_rec:dockerfile" .

Run docker container: docker run -d -p 8080:8080 -i -t --name imgrec img_rec:dockerfile java -jar target/ImageRecognizerService-1.0-SNAPSHOT.jar
