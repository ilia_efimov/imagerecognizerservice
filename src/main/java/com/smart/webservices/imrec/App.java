package com.smart.webservices.imrec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


@Configuration
@EnableAutoConfiguration
@ComponentScan
public class App {
    public static void main(String[] args) throws IOException {
        //loadDll("libtensorflow_jni.so");
        SpringApplication.run(new Class[]{App.class}, args);
    }

    public static void loadDll(String name) throws IOException {
        InputStream in = App.class.getClassLoader().getResourceAsStream(name);
        byte[] buffer = new byte[1024];
        int read;
        File temp = File.createTempFile(name, "");
        FileOutputStream fos = new FileOutputStream(temp);

        while((read = in.read(buffer)) != -1) {
            fos.write(buffer, 0, read);
        }
        fos.close();
        in.close();

        System.load(temp.getAbsolutePath());
    }
}
