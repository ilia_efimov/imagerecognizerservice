package com.smart.webservices.imrec.processing;

import java.io.Serializable;

public class RecognizedObject implements Serializable {
    private final String objectName;
    private final float rate;

    public RecognizedObject(String objectName, float rate) {
        this.objectName = objectName;
        this.rate = rate;
    }

    public String getObjectName() {
        return objectName;
    }

    public float getRate() {
        return rate;
    }
}
