package com.smart.webservices.imrec.processing.controller;

import com.smart.webservices.imrec.processing.RecognizedObject;
import com.smart.webservices.imrec.processing.tf.recognizer.object.ImageRecognizer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.smart.webservices.imrec.processing.tf.recognizer.utils.IOUtils.readModel;
import static com.smart.webservices.imrec.processing.tf.recognizer.utils.IOUtils.readFeatureFile;

@RestController
@RequestMapping(value = "/rest")
public class ImageRecognizeController {

    private static final String TF_MODEL_GRAPH_PATH = "tensorflow_inception_graph.pb";
    private static final String LABELS_FILE_PATH = "imagenet_comp_graph_label_strings.txt";

    private ImageRecognizer imageRecognizer;

    @PostConstruct
    public void init() {
        byte[] graphDef = readModel(TF_MODEL_GRAPH_PATH);
        List<String> labels = readFeatureFile(LABELS_FILE_PATH);
        imageRecognizer = ImageRecognizer.of(graphDef, labels);
    }

    @RequestMapping(value = "/recognize", method = RequestMethod.POST)
    @ResponseBody
    public RecognizedObject recognize(@RequestBody final byte[] data) {
        return imageRecognizer.recognize(data);
    }
}
