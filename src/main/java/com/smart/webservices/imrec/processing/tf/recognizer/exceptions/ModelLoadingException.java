package com.smart.webservices.imrec.processing.tf.recognizer.exceptions;

public class ModelLoadingException {
    private ModelLoadingException() {
    }

    public static RuntimeException cantLoadModel(String fileName) {
        return new RuntimeException(String.format("Can't load a model from file %s", fileName));
    }

    public static RuntimeException cantLoadFeatureFile(String fileName) {
        return new RuntimeException(String.format("Can't load the file %s with features", fileName));
    }
}
