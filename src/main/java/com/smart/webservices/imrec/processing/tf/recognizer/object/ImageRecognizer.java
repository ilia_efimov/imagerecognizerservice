package com.smart.webservices.imrec.processing.tf.recognizer.object;

import com.smart.webservices.imrec.processing.RecognizedObject;
import com.smart.webservices.imrec.processing.tf.recognizer.utils.GraphBuilder;
import org.tensorflow.Graph;
import org.tensorflow.Output;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

public class ImageRecognizer {
/**
    Some constants specific to the pre-trained model at:
    https://storage.googleapis.com/download.tensorflow.org/models/inception5h.zip
    - The model was trained with images scaled to 224x224 pixels.
    - The colors, represented as R, G, B in 1-byte each were converted to
    float using (value - Mean)/Scale.
 **/
    private static final int H = 224;
    private static final int W = 224;
    private static final float MEAN = 117f;
    private static final float SCALE = 1f;

    private final byte[] graphDef;
    private final List<String> labels;

    private ImageRecognizer(byte[] graphDef, List<String> labels) {
        this.graphDef = graphDef;
        this.labels = labels;
    }

    public static ImageRecognizer of(byte[] graphDef, List<String> labels) {
        return new ImageRecognizer(graphDef, labels);
    }

    public RecognizedObject recognize(@NotNull final byte[] data) {
        try (Tensor<Float> image = normalizeImage(data)) {
            float[] labelProbabilities = recognize(graphDef, image);
            int bestLabelIdx = maxIndex(labelProbabilities);
            return new RecognizedObject(labels.get(bestLabelIdx), labelProbabilities[bestLabelIdx] * 100f);
        }
    }

    private static float[] recognize(byte[] graphDef, Tensor<Float> image) {
        try (Graph g = new Graph()) {
            g.importGraphDef(graphDef);
            try (Session s = new Session(g);
                 Tensor<Float> result =
                         s.runner().feed("input", image).fetch("output").run().get(0).expect(Float.class)) {
                final long[] rshape = result.shape();
                if (result.numDimensions() != 2 || rshape[0] != 1) {
                    throw new RuntimeException(
                            String.format(
                                    "Expected model to produce a [1 N] shaped tensor where N is the number of labels, instead it produced one with shape %s",
                                    Arrays.toString(rshape)));
                }
                int nlabels = (int) rshape[1];
                return result.copyTo(new float[1][nlabels])[0];
            }
        }
    }

    private static Tensor<Float> normalizeImage(byte[] imageBytes) {
        try (Graph graph = new Graph()) {
            GraphBuilder b = GraphBuilder.of(graph);
            // Since the graph is being constructed once per execution here, we can use a constant for the
            // input image. If the graph were to be re-used for multiple input images, a placeholder would
            // have been more appropriate.
            final Output<String> input = b.constant("input", imageBytes);
            final Output<Float> output =
                    b.div(
                            b.sub(
                                    b.resizeBilinear(
                                            b.expandDims(
                                                    b.cast(b.decodeJpeg(input, 3), Float.class),
                                                    b.constant("make_batch", 0)),
                                            b.constant("size", new int[]{H, W})),
                                    b.constant("MEAN", MEAN)),
                            b.constant("SCALE", SCALE));
            try (Session s = new Session(graph)) {
                return s.runner().fetch(output.op().name()).run().get(0).expect(Float.class);
            }
        }
    }

    private static int maxIndex(float[] probabilities) {
        int best = 0;
        for (int i = 1; i < probabilities.length; ++i) {
            if (probabilities[i] > probabilities[best]) {
                best = i;
            }
        }
        return best;
    }
}
