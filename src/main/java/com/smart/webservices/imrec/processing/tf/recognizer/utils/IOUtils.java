package com.smart.webservices.imrec.processing.tf.recognizer.utils;

import com.smart.webservices.imrec.processing.tf.recognizer.object.ImageRecognizer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import static com.smart.webservices.imrec.processing.tf.recognizer.exceptions.ModelLoadingException.cantLoadModel;
import static com.smart.webservices.imrec.processing.tf.recognizer.exceptions.ModelLoadingException.cantLoadFeatureFile;

public class IOUtils {
    public static byte[] readModel(String name) {
        try {
            InputStream in = Optional.ofNullable(ImageRecognizer.class.getClassLoader().getResourceAsStream(name))
                    .orElseThrow(() -> cantLoadModel(name));
            return org.apache.commons.io.IOUtils.toByteArray(in);
        } catch (IOException e) {
            throw cantLoadModel(name);
        }
    }

    public static List<String> readFeatureFile(String name) {
        try {
            InputStream in = Optional.ofNullable(ImageRecognizer.class.getClassLoader().getResourceAsStream(name))
                    .orElseThrow(() -> cantLoadFeatureFile(name));
            return org.apache.commons.io.IOUtils.readLines(in, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw cantLoadFeatureFile(name);
        }
    }
}
