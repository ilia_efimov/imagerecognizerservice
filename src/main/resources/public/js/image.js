function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var readerForUploading = new FileReader();
    reader.onload = function(e) {
      $('#imgPreview').attr('src', e.target.result);
    }

    readerForUploading.onload = function(e) {
       $.ajax({
             url: '/rest/recognize',
             type: 'POST',
             contentType: 'application/octet-stream',
             data: e.srcElement.result,
             processData: false,
             success: function(result) {
                $("#result").html('Изображен: ' + result['objectName'] + ' с вероятностью ' + result['rate'] + '%');
             }
       });
    }
    reader.readAsDataURL(input.files[0]);
    readerForUploading.readAsArrayBuffer(input.files[0]);
  }
}

$("#imgInput").change(function() {
  readURL(this);
});
